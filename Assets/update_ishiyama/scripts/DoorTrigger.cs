﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{
    MeshRenderer renderer;
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
           renderer = this.gameObject.GetComponent<MeshRenderer>();
           renderer.enabled = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            renderer.enabled = true;
        }
    }
}
