﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomDoorTrigger : MonoBehaviour {
    public MeshRenderer renderer;
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            renderer.enabled = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            renderer.enabled = true;
        }
    }
}
