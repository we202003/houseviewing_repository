﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosetTrigger : MonoBehaviour {

    public MeshRenderer closetRenderer;
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            closetRenderer.enabled = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            closetRenderer.enabled = true;
        }
    }
}
