﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    public float speed = 1.0f;
    Vector3 beforePlayerPos;
    Animator animator;
    CharacterController controller;
    void Start()
    {
        animator = GetComponent<Animator>();
        beforePlayerPos = transform.position; //ゲーム開始時のプレイヤーの位置情報取得 //回転関連で使う
        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetBool("isWalk", false);

        float moveSpeedX = Input.GetAxis("Horizontal") * Time.deltaTime * speed;//移動関連
        float moveSpeedZ = Input.GetAxis("Vertical") * Time.deltaTime * speed;//移動関連
        controller.Move(transform.position += new Vector3(moveSpeedX, 0, moveSpeedZ));
        //Debug.Log(" position =" + transform.position);
        Vector3 sabun = transform.position - beforePlayerPos;// 回転関連 //移動した場所と移動前の場所。どれだけ変わってますか？これで移動した方向と距離がわかります

        Debug.Log("sabun =" + sabun);
        if (sabun.sqrMagnitude > 0.0f)//
        {
            transform.rotation = Quaternion.LookRotation(sabun);//回転関連
        }//

        beforePlayerPos = transform.position;
        if (moveSpeedZ != 0.0f || moveSpeedX != 0.0f)//移動時関連
        {
            animator.SetBool("isWalk", true);
        }

    }
}

