﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoom : MonoBehaviour {
    private Camera cam;

	void Start () {
        cam = GetComponent<Camera>();
	}
	
	void Update () {
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        float view = cam.fieldOfView - scroll * 10f;

        cam.fieldOfView = Mathf.Clamp(value: view, min: 0.1f, max: 60f);

        ResetCameraZoom();
	}

    private void ResetCameraZoom()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            cam.fieldOfView = 60f;
        }
    }
}
