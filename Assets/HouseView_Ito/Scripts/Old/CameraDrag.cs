﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDrag : MonoBehaviour {
    private Camera cam;

    // カメラの回転速度を格納する変数
    public Vector2 rotationSpeed;

    // マウス座標を格納する変数
    private Vector2 lastMousePosition;
    // カメラの角度を格納する変数（初期値に0,0を代入）
    private Vector2 newAngle = new Vector2(0, 0);
    
    // カメラの初期位置を格納する変数
    private Vector2 initialCameraRotation;

    void Start()
    {
        cam = GetComponent<Camera>();
        //カメラの初期アングルを保存
        //initialCameraRotation = this.gameObject.transform.localEulerAngles;
        initialCameraRotation = cam.transform.localEulerAngles;
    }

    void Update()
    {
        CameraDragDrop();
        ScrollZoom();
        ResetCamera();
    }

    private void CameraDragDrop()
    {
        if (Input.GetMouseButtonDown(0))
        {
            // カメラの角度を変数"newAngle"に格納
            newAngle = cam.transform.eulerAngles;
            // マウス座標を変数"lastMousePosition"に格納
            lastMousePosition = Input.mousePosition;
        }
        else if (Input.GetMouseButton(0))
        {
            // Y軸の回転：マウスドラッグ逆方向に視点回転
            // マウスの水平移動値に変数"rotationSpeed"を掛ける
            //（クリック時の座標とマウス座標の現在値の差分値）
            newAngle.y -= (Input.mousePosition.x - lastMousePosition.x) * rotationSpeed.x;
            newAngle.x -= (lastMousePosition.y - Input.mousePosition.y) * rotationSpeed.y;

            // "newAngle"の角度をカメラ角度に格納
            cam.gameObject.transform.eulerAngles = newAngle;
            // マウス座標を変数"lastMousePosition"に格納
            lastMousePosition = Input.mousePosition;
        }
    }

    private void ScrollZoom()
    {
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        float view = cam.fieldOfView - scroll * 10f;

        cam.fieldOfView = Mathf.Clamp(value: view, min: 0.1f, max: 60f);
    }

    private void ResetCamera()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            cam.transform.localEulerAngles = initialCameraRotation;
            cam.fieldOfView = 60f;
        }
    }
}
