﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControll : MonoBehaviour {
    private GameObject mainCamera;
    private GameObject subCamera;

	void Start () {
        mainCamera = GameObject.FindWithTag("MainCamera");
        subCamera = GameObject.FindWithTag("SubCamera");

        subCamera.SetActive(false);
	}
	
	void Update () {

        if (Input.GetKey("space"))
        {
            mainCamera.SetActive(false);
            subCamera.SetActive(true);
        }
        else
        {
            subCamera.SetActive(false);
            mainCamera.SetActive(true);
        }
	}
}
