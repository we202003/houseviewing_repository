﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraSwitching : MonoBehaviour {

    private GameObject mainCamera;
    private GameObject subCamera;

    public GameObject MenuPanel;
    public Camera[] cameras;

    //public Camera mainCamera;
    //public Camera backYard;
    //public Camera exteriorDriveway;
    //public Camera frontYard;
    //public Camera interiorKitchen;
    //public Camera interiorLivingRoom;
    //public Camera camera3D;
    void Start()
    {
        mainCamera = GameObject.FindWithTag("MainCamera");
        subCamera = GameObject.FindWithTag("SubCamera");

        subCamera.SetActive(false);

        //初期カメラは配列0番目を表示させる
        cameras[0].enabled = true;
        for (int i = 1; i < cameras.Length; i++)
        {
            cameras[i].enabled = false;
        }
    }

    void Update()
    {
        //スペースキーでUnityChanの視点切り替えが可能
        if (Input.GetKey("space"))
        {
            mainCamera.SetActive(false);
            subCamera.SetActive(true);
        }
        else
        {
            subCamera.SetActive(false);
            mainCamera.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.M))
        {
            MenuPanel.SetActive(!MenuPanel.activeSelf);
        }
    }

    //buttonのクリックイベントごとに引数を設定
    //指定のボタンだけtrue、それ以外はfalseにする
    public void OnSwitchView(int index)
    {
        cameras[index].enabled = true;

        for (int i = 0; i < cameras.Length; i++)
        {
            if (i != index)
            {
                cameras[i].enabled = false;
            }
        }
    }

    //初期画面に戻る
    public void OnResetScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }


}
