﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCtrl : MonoBehaviour {
    public Vector2 rotationSpeed;

    private Vector2 lastMousePosition;
    private Vector2 newAngle = new Vector2(0, 0);

    private Transform camTransform;
    private Vector2 initialCameraRotation;

    void Start()
    {
        camTransform = this.gameObject.transform;
        initialCameraRotation = this.gameObject.transform.localEulerAngles;
    }

    void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            newAngle = this.gameObject.transform.eulerAngles;
            lastMousePosition = Input.mousePosition;
        }
        else if (Input.GetMouseButton(0))
        {
            newAngle.y -= (Input.mousePosition.x - lastMousePosition.x) * rotationSpeed.x;
            newAngle.x -= (lastMousePosition.y - Input.mousePosition.y) * rotationSpeed.y;
            this.gameObject.transform.eulerAngles = newAngle;

            lastMousePosition = Input.mousePosition;
        }
        
        ResetCameraRotations();
    }

    private void ResetCameraRotations()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            this.gameObject.transform.localEulerAngles = initialCameraRotation;
            Debug.Log(initialCameraRotation);
        }
    }
}
