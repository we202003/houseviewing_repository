﻿//作成：伊藤

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ViewAndUIManager : MonoBehaviour {

    private GameObject mainCamera;
    private GameObject subCamera;

    public GameObject MenuPanel;
    //2020.04.06 ObjectCamera機能追加のため改修 伊藤 ---
    public GameObject exteriorCamerapanel;
    public Camera exteriorCamera;
    //--------------------------------------------

    public UnityChanSwitch uniSwitch;
    
    //2020.04.03 使用モデル変更のため改修 和田 ---
    //public GameObject roof;
    public GameObject[] roofs;
    //--------------------------------------------
    public Camera[] cameras;

    //2020.04.07 山條さん要望のため改修 伊藤 ---
    public Text roomText;
    //--------------------------------------------

    void Start()
    {
        mainCamera = GameObject.FindWithTag("MainCamera");
        subCamera = GameObject.FindWithTag("SubCamera");

        subCamera.SetActive(false);
        //exteriorCamera.gameObject.SetActive(false);
        //exteriorCamerapanel.SetActive(false);

        //初期カメラは配列0番目を表示させる
       
        for (int i = 1; i < cameras.Length; i++)
        {
            cameras[i].enabled = false;
        }
        cameras[0].enabled = true;
    }

    void Update()
    {
        //Camera型で管理しているcameras[].enabledと
        //GameObject型で管理しているmainCamera.SetActiveの違い
        if (cameras[0].enabled)
        {
            //CでUnityChanの視点切り替えが可能
            if (Input.GetKeyDown(KeyCode.C))
            {
                mainCamera.SetActive(!mainCamera.activeSelf);
                subCamera.SetActive(!subCamera.activeSelf);
            }
        }

        //メニューパネルの表示・非表示
        if (Input.GetKeyDown(KeyCode.M))
        {
            if (exteriorCamerapanel.activeSelf == false)
            {
                MenuPanel.SetActive(!MenuPanel.activeSelf);
            }
        }
    }

    //buttonのクリックイベントごとに引数を設定
    //指定のボタンだけtrue、それ以外はfalseにする
    public void OnSwitchView(int index)
    {
        for (int i = 0; i < cameras.Length; i++)
        {
            if (i != index)
            {
                cameras[i].enabled = false;
            }
        }
        cameras[index].enabled = true;

        //2020.04.07 山條さん要望のため改修  伊藤 ---
        //現在位置の表示textがUnityChanViewの場合true
        //それ以外はfalse
        if (index == 0 || index == cameras.Length - 1)
        {
            roomText.enabled = true;
        }
        else
        {
            roomText.enabled = false;
        }
        //--------------------------------------------


        //2020.04.07 自動走行機能と自由走行機能の切り替え  湯沢 ---
        //cameras配列の最後にMainCameraを設置しボタンの引数を配列最後の数に設定
        if (index == cameras.Length - 1)
        {
            uniSwitch.SetAutoMode();
        }
        else
        {
            uniSwitch.SetFreeMode();
        }
        //--------------------------------------------
    }

    public void OnRoofButtonClicked()
    {
        //2020.04.03 使用モデル変更のため改修 和田 ---
        //roof.SetActive(!roof.activeSelf);
        foreach(GameObject r in roofs)
        {
            r.SetActive(!r.activeSelf);
        }
        //--------------------------------------------

    }

    //2020.04.06 ObjectCamera機能追加のため改修 伊藤 ---
    //
    public void OnAroundViewButtonClicked()
    {
        MenuPanel.SetActive(false);
        //for (int i = 0; i < cameras.Length; i++)
        //{
        //    cameras[i].enabled = false;
        //}
        //subCamera.SetActive(false);

        exteriorCamerapanel.SetActive(true);
        exteriorCamera.gameObject.SetActive(true);

    }

    //--------------------------------------------

    //初期画面に戻る
    public void OnResetScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

}
