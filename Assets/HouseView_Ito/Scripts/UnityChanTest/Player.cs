﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    Rigidbody rb;
    public float speed = 3f;
    private Animator animator;
    Vector3 playerPos;
    bool ground;

	void Start () {
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        playerPos = transform.position;
	}
	
	void Update () {
        if (ground)
        {
            float x = Input.GetAxisRaw("Horizontal") * Time.deltaTime * speed;
            float z = Input.GetAxisRaw("Vertical") * Time.deltaTime * speed;

            rb.MovePosition(transform.position + new Vector3(x, 0, z));
            Vector3 direction = transform.position - playerPos;

            if (direction.magnitude > 0.01f)
            {
                transform.rotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
                animator.SetBool("is_running", true);
            }
            else
            {
                animator.SetBool("is_running", false);
            }

            playerPos = transform.position;
            
        }
        
	}
}
