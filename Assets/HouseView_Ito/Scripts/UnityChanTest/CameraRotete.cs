﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotete : MonoBehaviour {

    private GameObject player;

	void Start () {
        player = GameObject.Find("unitychan");
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            transform.RotateAround(player.transform.position, Vector3.up, -5f);
        }
        else if (Input.GetKey(KeyCode.RightShift))
        {
            transform.RotateAround(player.transform.position, Vector3.up, 5f);
        }
	}
}
