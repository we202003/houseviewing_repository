﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityChanDemo1 : MonoBehaviour {
    private Animator animator;

	void Start () {
        animator = GetComponent<Animator>();
    }
	
	void Update () {

        if (Input.GetKey("up"))
        {
            transform.position += transform.forward * 0.3f;
            animator.SetBool("is_running", true);
        }
        else
        {
            animator.SetBool("is_running", false);
        }

        if (Input.GetKey("down"))
        {
            transform.position -= transform.forward * 0.1f;
            animator.SetBool("is_backking", true);
        }
        else
        {
            animator.SetBool("is_backking", false);
        }

        if (Input.GetKey("right"))
        {
            transform.Rotate(0, 10, 0);
        }

        if (Input.GetKey("left"))
        {
            transform.Rotate(0, -10, 0);
        }
        
	}
}
