﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoPlayerMoveing : MonoBehaviour
{
    CharacterController controller; //unityちゃんにcharactorコントローラーをつけておいてね
    Animator animator;

    public Collider[] goalPoints; //シーンにメッシュレンダラーをオフにしたboxを置いてアタッチしてね
    public float speed = 0.1f;//いどするスピードここで制御

    //float gravity = 0.2f; //地面についていないなら重力付けます
    bool isAutoMode = true; //ここtrueにすれば動くよ //自由走行との切り替えの仕方の一つとしてどうかと思い制作。一応すぐ消せます。//動作確認のためtrueにしてます
    Vector3 cameraPosition;
    public Vector3 CameraPosition //カメラに追従させるためのゲッター
    {
        get { return cameraPosition; }
    }

    Quaternion oldrotation;
    Quaternion updateRotation;
    Quaternion slerpRotation;

    public float slerpSpeed = 20.0f;//triggerヒットした時の回転速度
    public bool isMove = true; //初動
    void Start()
    {

        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        if (isAutoMode == true)
        {
            oldrotation = this.transform.rotation;
            updateRotation = Quaternion.LookRotation(goalPoints[0].transform.position - this.transform.position);
            //slerpRotation = Quaternion.Slerp(oldrotation, updateRotation, 0.5f);
            slerpRotation = Quaternion.LookRotation(goalPoints[0].transform.position - this.transform.position);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isAutoMode == true)
        {
            if (isMove == true)
            {
                animator.SetBool("is_running", true);
                transform.rotation = slerpRotation;
                //transform.position += transform.forward * speed;
                controller.Move(transform.forward * speed);
            }
            if (isMove == false)
            {
                animator.SetBool("is_running", false);
            }
                cameraPosition = this.transform.position;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        for (int point = 0; point < goalPoints.Length; point++)
        {
            if (other == goalPoints[point])
            {
                Debug.Log("実行");
                if (point == goalPoints.Length - 1) //index最後のコライダーだったら動きを止める
                {
                    Debug.Log(isMove);
                    isMove = false;
                }

                if (point < goalPoints.Length - 1) //indexの最後のいっこ前まではLookRotationを使って前のオブジェクトをLookしながら移動する
                {
                    Debug.Log("検知");
                    Vector3 targetPos = goalPoints[point + 1].transform.position;
                    oldrotation = this.transform.rotation;
                    updateRotation = Quaternion.LookRotation(targetPos - this.transform.position);
                    slerpRotation = Quaternion.Slerp(oldrotation, updateRotation, Time.deltaTime * slerpSpeed);

                }
            }
        }
    }
}

