﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityChanSwitch : MonoBehaviour {

    public GameObject unityChan;

    AutoMoving auto;
    UnityChanController free;

    private void Start()
    {
        auto = unityChan.GetComponent<AutoMoving>();
        free = unityChan.GetComponent<UnityChanController>();
        this.SetFreeMode();
    }

    public void SetAutoMode()
    {
        if (!auto.enabled)
        {
            auto.enabled = true;
            auto.Reset();
            free.enabled = false;
        }        
    }

    public void SetFreeMode()
    {
        free.enabled = true;
        auto.Close();
        auto.enabled = false;
    }


}
