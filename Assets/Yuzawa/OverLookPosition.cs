﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverLookPosition : MonoBehaviour {

    public AutoMoving auto;
    public float startRotation;

    private void OnTriggerEnter(Collider other)
    {
        auto.OverLook(startRotation);
    }
}
