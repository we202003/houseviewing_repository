﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoMoving : MonoBehaviour {

    //UnityChan自動走行モードscript
    //PointTriggerスクリプトを持つオブジェクトすべてにアタッチ
    public Transform[] points;
    public float speed;
    public float roteSpeed;
    public float overLookSpeed;
    public float overLookRange;

    CharacterController con;
    Transform camera;
    Animator anim;
    Quaternion targetRota;
    Vector3 direction;
    Quaternion angle;
    
    public bool Is_OverLook;
    int index;
    int lRIndex;
    float[] lRs;
    float overLookStartRotation;
    float val;
    bool is_Auto;
    public UnityChanSwitch unityChanSwitch;

    private void Awake()
    {
        camera = gameObject.transform.Find("MainCamera");
    }

    void Start () {
        anim = GetComponent<Animator>();            
        con = GetComponent<CharacterController>();     
        lRs = new float[] { 1, -1, -1, 1 };
        val = overLookRange;
    }
	
	void Update () {
        enabled = is_Auto;

        if (index < points.Length && !Is_OverLook)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRota, Time.deltaTime * roteSpeed);
            direction.y -= 9.8f * Time.deltaTime;
            con.Move(direction * (speed * Time.deltaTime));
            anim.SetBool("is_running", true);
        }
        if(index == points.Length)
        {
            anim.SetBool("is_running", false);
        }
        if (Is_OverLook)
        {
            if (overLookStartRotation > 0)
            {
                transform.rotation = Quaternion.Euler(0, overLookStartRotation, 0);
                overLookStartRotation = 0;
            } else if (val > 0)
            {     
                transform.Rotate(0, (overLookSpeed * lRs[lRIndex]) * Time.deltaTime, 0);
                val -= (overLookSpeed * Time.deltaTime);
            }
            else
            {
                lRIndex++;
                val = overLookRange;
                if (lRIndex == lRs.Length)
                {
                    lRIndex = 0;
                    Is_OverLook = false;
                }
            }
            anim.SetBool("is_running", false);
        }

    }

    public void TargetUpdate()
    {
        index++;
        //-----------------------------------------
        if (index >= points.Length)
        {
            unityChanSwitch.SetFreeMode();
            Debug.Log(enabled);
        }
        //-----------------------------------------
        if (index < points.Length && is_Auto)
        {
            targetRota = Quaternion.LookRotation(points[index].position - transform.position);
            direction = points[index].position - transform.position;
            direction.Normalize();
        }
 
    }

    public void OverLook(float startRotation)
    {
        if (is_Auto)
        {
            overLookStartRotation = startRotation;
            Is_OverLook = true;
        }
    }

    public void Reset()
    {
        is_Auto = true;
        index = 0;
        lRIndex = 0;
        transform.position = points[0].position;
        transform.rotation = Quaternion.Euler(0, 180, 0);
        camera.localPosition = new Vector3(0, 1.8f, -0.5f);
        targetRota = Quaternion.LookRotation(points[index].position - transform.position);
        direction = points[index].position - transform.position;
        direction.Normalize();
        
    }

    public void Close()
    {
        camera.localPosition = new Vector3(0, 1.5f, -2);
        is_Auto = false;
        Is_OverLook = false;
    }
}
