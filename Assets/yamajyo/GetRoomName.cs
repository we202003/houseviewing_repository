﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GetRoomName : MonoBehaviour {
    //作成者山條
    //ユーザー（今回はUnityChan）がどの場所にいるかをUIで表示させるスクリプト
    //各部屋の移動ポジションのオブジェクトにアタッチし
    //各場所タグ名を変更すること

    public Text roomText;

    //2020.04.03 位置表示機能追加のため改修 和田 ---
    public GameObject floorPlan;
    List<Image> roomList = new List<Image>();
    //----------------------------------------------


    //2020.04.03 位置表示機能追加のため改修 和田 ---
    private void Awake()
    {
        floorPlan.GetComponentsInChildren<Image>(roomList);
    }
    //----------------------------------------------

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            roomText.text = "";

            //2020.04.03 位置表示機能追加のため改修 和田 ---
            foreach (Image i in roomList.Where(n => n.color == Color.red))
            {
                i.color = Color.white;
            }
            //----------------------------------------------
        }


    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            roomText.text = this.gameObject.tag;

            //2020.04.03 位置表示機能追加のため改修 和田 ---
            foreach (Image i in roomList.Where(n => n.name == this.gameObject.tag))
            {
                i.color = Color.red;
            }
            //----------------------------------------------
        }
    }
}
