﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRoom : MonoBehaviour {
    public GameObject unityChan;
    public GameObject bedRoom1;
    public GameObject bedRoom2;
    public GameObject living;
    public GameObject kitchen;
    public GameObject masterBedRoom;

    Vector3 bedRoom1Pos;
    Vector3 bedRoom2Pos;
    Vector3 livingPos;
    Vector3 kitchenPos;
    Vector3 masterBedRoomPos;




    void Start () {
        bedRoom1Pos = bedRoom1.transform.position;
        bedRoom2Pos = bedRoom2.transform.position;
        livingPos = living.transform.position;
        kitchenPos = kitchen.transform.position;
        masterBedRoomPos = masterBedRoom.transform.position;

		
	}
	
    public void MoveBedRoom1()
    {
        unityChan.transform.position = bedRoom1Pos;
    }

    public void MoveBedRoom2()
    {
        unityChan.transform.position = bedRoom2Pos; ;
    }

    public void MoveLiving()
    {
        unityChan.transform.position = livingPos;
    }

    public void MoveKitchen()
    {
        unityChan.transform.position = kitchenPos;
    }

    public void MoveMasterBedRoom()
    {
        unityChan.transform.position = masterBedRoomPos;
    }
}
