﻿//作成：和田

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForStairs : MonoBehaviour {

    //うまく動かないようなら階段・階段通路も入れるので配列にしておく
    public GameObject[] stairs;

    private void OnTriggerEnter(Collider other)
    {
     if(other.name == "unitychan")
        {
            foreach(GameObject s in stairs)
            {
                s.GetComponent<MeshCollider>().enabled = false;
            }
        }   
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name == "unitychan")
        {
            foreach (GameObject s in stairs)
            {
                s.GetComponent<MeshCollider>().enabled = true;
            }
        }

    }
}
