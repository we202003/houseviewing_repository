﻿//作成：和田

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityChanController : MonoBehaviour {

    CharacterController controller;
    Animator animator;
    Vector3 moveDirection = Vector3.zero;
    public float speedMove;

    void Start()
    {
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();

    }

    void Update()
    {
        moveDirection.z = Input.GetAxis("Vertical") * speedMove;

        if(controller.isGrounded == false)
        {
            moveDirection.y -= 9.8f * Time.deltaTime;
        }
        else
        {
            moveDirection.y = 0;
        }
        
        //方向転換
        transform.Rotate(0, Input.GetAxis("Horizontal") * speedMove, 0);

        //移動実行
        Vector3 globalDirection = transform.TransformDirection(moveDirection);
        controller.Move(globalDirection * Time.deltaTime);

        //伊藤さんのUnityちゃん用に変更
        //animator.SetBool("isWalk", moveDirection.z != 0.0f);
        animator.SetBool("is_running", moveDirection.z != 0.0f);

    }
}
