﻿//作成：和田

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExteriorCamera : MonoBehaviour {

    public GameObject exteriorCamerapanel;
    public GameObject menuPanel;
    public UnityChanController unityChanController;
    public float speed;
    public float changeAmout;
    public float limitUpAngle;
    public float limitDownAngle;
    Camera cameraComp;
    Vector3 centerPosition;
    Vector3 defaultPosition;
    Vector3 defaultAngle;
    float defaultFOV;
    float moveAngle;

    bool onMoveLeft;
    bool onMoveRight;
    bool onZoomIn;
    bool onZoomOut;
    bool onAngleUp;
    bool onAngleDown;

    bool autoOnOff;

    void Start()
    {
        centerPosition = new Vector3(10, 3, 0);
        cameraComp = GetComponent<Camera>();
        defaultPosition = transform.position;
        defaultFOV = cameraComp.fieldOfView;
        defaultAngle = transform.rotation.eulerAngles;
        moveAngle = Mathf.Floor(defaultAngle.x);
    }

    void Update()
    {
        if (autoOnOff)
        {
            MoveCamera(0.1f);
            if(this.transform.position == defaultPosition)
            {
                autoOnOff = false;
            }            
        }

        //Unityちゃんのコントローラを切る
        //伊藤さんのUIのボタンに入れるなら消す
        if(unityChanController.enabled == true)
        {
            unityChanController.enabled = false;
        }

        //回転
        if (Input.GetButton("Horizontal"))
        {
            MoveCamera(Input.GetAxis("Horizontal"));
        }

        //角度
        if (Input.GetKeyDown(KeyCode.V))
        {
            ResetAngle();
        }
        else if (Input.GetAxis("Vertical") > 0)
        {
            UpAngle();
        }
        else if(Input.GetAxis("Vertical") < 0) {
            DownAngle();
        }

        //拡大縮小
        if (Input.GetKeyDown(KeyCode.Z))
        {
            ResetZoom();
        }
        else if (Input.GetKey(KeyCode.U))
        {
            ZoomIn();
        }
        else if (Input.GetKey(KeyCode.L))
        {
            ZoomOut();
        }

        //リセット
        if (Input.GetKeyDown(KeyCode.R))
        {
            AllReset();
        }

        //メニューの表示非表示
        if (Input.GetKeyDown(KeyCode.E))
        {
            exteriorCamerapanel.SetActive(!exteriorCamerapanel.activeSelf);
        }


        //ボタン押しっぱなし対応
        if (onMoveLeft)
        {
            MoveCamera(-changeAmout);
        }else if (onMoveRight)
        {
            MoveCamera(changeAmout);
        }

        if (onZoomIn)
        {
            ZoomIn();
        }else if (onZoomOut)
        {
            ZoomOut();
        }

        if (onAngleUp)
        {
            UpAngle();
        }else if (onAngleDown)
        {
            DownAngle();
        }

    }

    public void PointerDownMoveLeft()
    {
        onMoveLeft = true;
    }
    public void PointerUpMoveLeft()
    {
        onMoveLeft = false;
    }

    public void PointerDownMoveRight()
    {
        onMoveRight = true;
    }
    public void PointerUpMoveRight()
    {
        onMoveRight = false;
    }

    void MoveCamera(float move)
    {
        //回転させる角度
        float angle = move * speed;

        //カメラを回転させる
        this.transform.RotateAround(centerPosition, Vector3.up, angle);
    }

    public void ResetZoom()
    {
        cameraComp.fieldOfView = defaultFOV;
    }

    public void PointerDownZoomIn()
    {
        onZoomIn = true;
    }
    public void PointerUpZoomIn()
    {
        onZoomIn = false;
    }

    void ZoomIn()
    {
        if (cameraComp.fieldOfView > 30)
        {
            cameraComp.fieldOfView--;
        }
    }

    public void PointerDownZoomOut()
    {
        onZoomOut = true;
    }
    public void PointerUpZoomOut()
    {
        onZoomOut = false;
    }

    void ZoomOut()
    {
        if (cameraComp.fieldOfView < 80)
        {
            cameraComp.fieldOfView++;
        }
    }

    public void ResetAngle()
    {
        Vector3 angle = cameraComp.transform.eulerAngles;
        angle.x = defaultAngle.x;
        transform.eulerAngles = angle;
        moveAngle = Mathf.Floor(defaultAngle.x);
    }

    public void PointerDownAngleUp()
    {
        onAngleUp = true;
    }
    public void PointerUpAngleUp()
    {
        onAngleUp = false;
    }

    void UpAngle()
    {
        ChangeAngle(-changeAmout);
    }

    public void PointerDownAngleDown()
    {
        onAngleDown = true;
    }
    public void PointerUpAngleDown()
    {
        onAngleDown = false;
    }

    void DownAngle()
    {
        ChangeAngle(changeAmout);
    }

    void ChangeAngle(float changeValue)
    {
        if (changeValue < 0 && moveAngle > limitUpAngle 
            || changeValue > 0 && moveAngle < limitDownAngle)
        {
            moveAngle += changeValue;
            transform.Rotate(changeValue, 0, 0);
        }
    }

    public void CameraReset()
    {
        transform.position = defaultPosition;
        transform.eulerAngles = defaultAngle;
    }

    public void AllReset()
    {
        ResetZoom();
        CameraReset();
    }

    public void OnReturnButtonClicked()
    {
        exteriorCamerapanel.SetActive(false);
        menuPanel.SetActive(true);
        unityChanController.enabled = true;
        this.gameObject.SetActive(false);
    }

    public void OnAutoButtonClicked()
    {
        autoOnOff = !autoOnOff;        
    }
}
