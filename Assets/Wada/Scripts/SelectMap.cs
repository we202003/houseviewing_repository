﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectMap : MonoBehaviour {

    public GameObject floor1F;
    public GameObject floorB1;
    //***********************************************
    public GameObject unityChan;
    public GameObject moveRoomPosition;
    List<Transform> roomList = new List<Transform>();
    //***********************************************

    void Start () {
        floorB1.SetActive(false);
        moveRoomPosition.GetComponentsInChildren<Transform>(roomList);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            floor1F.SetActive(false);
            floorB1.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            floorB1.SetActive(false);
            floor1F.SetActive(true);
        }
    }

    //***********************************************
    public void ChangeMap()
    {
        floor1F.SetActive(!floor1F.activeSelf);
        floorB1.SetActive(!floorB1.activeSelf);
    }

    public void MoveRoom(Button button)
    {
        unityChan.transform.position = roomList.Find(n => n.name == button.name).position;
    }
    //***********************************************
}
